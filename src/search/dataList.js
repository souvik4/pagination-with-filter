import React, {useState, useEffect} from 'react';
import BootsrapTable from 'react-bootstrap-table-next'
import 'react-bootstrap-table2-paginator/dist/react-bootstrap-table2-paginator.min.css';
import paginationFactory from 'react-bootstrap-table2-paginator';
import filterFactory, { textFilter } from 'react-bootstrap-table2-filter';
import 'react-bootstrap-table2-filter/dist/react-bootstrap-table2-filter.min.css';

function DataList() {

    const [userList, setUserList] = useState([]);

    const columns = [
        
        {dataField: 'id', text: 'Id', filter: textFilter()},
        {dataField: 'name', text: 'Name', filter: textFilter()},
        {dataField: 'username', text: 'Username', filter: textFilter()},
        {dataField: 'email', text: 'Email',filter: textFilter()}
    ]

    const pagination = paginationFactory({
        page: 1,
        sizePerPage: 5, 
        lastPageText: '>>',
        firstPageText: '<<',
        nextPageText: '>',
        prePageText: '<',
        showTotal: true,
        alwaysShowAllBtns: true,
        onPageChange: function (page, sizePerPage){
            console.log('page', page);
            console.log('sizePerPage', sizePerPage);
        },
        onSizePerPageChange: function (page, sizePerPage){
            console.log('page', page);
            console.log('sizePerPage', sizePerPage);
        }
    });

    useEffect(() => {
        fetch('https://jsonplaceholder.typicode.com/users')
        .then(response => response.json())
        .then(result => setUserList(result))
        .catch(error => console.log(error))
    },[])

    return (
        <div>
            <BootsrapTable 
                keyField='id' 
                columns={columns} 
                data={userList} 
                pagination={ pagination} 
                filter={ filterFactory()}
            />
        </div>
    )
}

export default DataList;