import './App.css';
import DataList from './search/dataList';

function App() {
  return (
    <div className="App">
      <DataList />
    </div>
  );
}

export default App;
